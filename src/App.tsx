import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';

const HomePage = lazy(() => import('./pages/HomePage'));
const LoginPage = lazy(() => import('./pages/LoginPage'));
const RegisterPage = lazy(() => import('./pages/RegisterPage'));

function App() {
  return (
    <div className="App">
      <Router>
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <PrivateRoute isAuth={false} path="/login">
              <LoginPage />
            </PrivateRoute>
            <PrivateRoute isAuth={false} path="/register">
              <RegisterPage />
            </PrivateRoute>
            <PrivateRoute isAuth={true} path="/">
              <HomePage />
            </PrivateRoute>
          </Switch>
        </Suspense>
      </Router>
    </div>
  );
}

export default App;
