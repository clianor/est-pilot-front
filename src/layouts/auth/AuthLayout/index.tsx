/** @jsxRuntime classic */
/** @jsx jsx */
import React, { ChangeEvent, FormEvent } from 'react';
import { jsx, css } from '@emotion/react';
import em from '@emotion/styled';

import Button from '../../../components/Button';
import InputBox from '../../../components/Input/InputBox';
import Row from '../../../components/Layout/Row';

interface AuthLayoutProps {
  type: 'login' | 'register';
  form?: any;
  isLoading?: boolean;
  errMsg?: string;
  onChange?(e: ChangeEvent<HTMLInputElement>): void;
  onSubmit?(e: FormEvent<HTMLFormElement>): void;
}

export const AuthLayout = ({
  type,
  form,
  isLoading,
  errMsg,
  onChange,
  onSubmit,
}: AuthLayoutProps) => {
  const text = type === 'login' ? '로그인' : '회원가입';

  return (
    <div css={AuthLayoutStyle}>
      <h4>{text}</h4>
      <form onSubmit={onSubmit} css={FormStyle}>
        <Row>
          <InputBox
            type="email"
            id="email"
            name="email"
            onChange={onChange}
            value={form?.email}
            placeholder="이메일"
            required
          />
        </Row>
        <Row>
          <InputBox
            type="password"
            id="password"
            name="password"
            onChange={onChange}
            value={form?.password}
            placeholder="패스워드"
            required
          />
        </Row>
        {type === 'register' && (
          <div>
            <InputBox
              type="password"
              id="passwordConfirm"
              name="passwordConfirm"
              onChange={onChange}
              value={form?.passwordConfirm}
              placeholder="패스워드 확인"
              required
            />
          </div>
        )}
        {errMsg && <StyledErrorMessage>{errMsg}</StyledErrorMessage>}
        <Button type="submit" size="middle" isLoading={isLoading}>
          {text}
        </Button>
      </form>
    </div>
  );
};

const AuthLayoutStyle = css`
  display: block;
  max-width: 30rem;
  width: 40vw;
  padding: 2.25rem;
  box-shadow: 0 1px 5px 1px #e6e9f4;
  background-color: #ffffff;
  margin: 0 auto;

  & > h4 {
    font-size: 2.25rem;
    padding-bottom: 2rem;
    letter-spacing: 0.5rem;
    text-align: center;
    font-family: 'Black Han Sans', sans-serif;
  }

  @media (max-width: 960px) {
    width: 80vw;
  }
`;

const FormStyle = css`
  display: flex;
  flex-direction: column;

  & > div {
    display: flex;
    flex-direction: column;
    max-width: 100%;
    padding-bottom: 0.75rem;
  }

  & > div > label {
    font-size: 1.1rem;
    text-align: left;
    padding-bottom: 0.5rem;
  }
`;

const StyledErrorMessage = em.span`
  color: red;
  padding: 0.75rem 0;
`;
