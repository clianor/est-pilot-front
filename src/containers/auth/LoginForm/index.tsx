import React, { ChangeEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import authModule from '../../../store/moduels/auth';
import { AuthLayout } from '../../../layouts/auth/AuthLayout';
import { RootState } from '../../../store';
import useLogin, { useLoginProps } from '../../../hooks/auth/useLogin';

function LoginForm() {
  const history = useHistory();
  const dispatch = useDispatch();
  const authState = useSelector((state: RootState) => state.auth);
  const [form, setForm] = useState<useLoginProps>({ email: '', password: '' });
  const { onLogin } = useLogin(form);

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  useEffect(() => {
    dispatch(authModule.actions.initAction());
  }, []);

  useEffect(() => {
    const { state }: { state: any } = history.location;

    if (
      state?.prev !== '/register' &&
      authState.data?.success &&
      !authState?.user
    ) {
      dispatch(
        authModule.actions.meActions.request({
          loading: true,
          user: undefined,
        }),
      );
    } else if (authState.data?.success && authState?.user) {
      history.replace('/');
    }

    if (state) delete (history.location.state as any)['prev'];
  }, [authState.data?.success]);

  return (
    <AuthLayout
      type="login"
      form={form}
      isLoading={authState.loading}
      errMsg={authState.errMsgs && authState.errMsgs[0]}
      onSubmit={onLogin}
      onChange={onChange}
    />
  );
}

export default LoginForm;
