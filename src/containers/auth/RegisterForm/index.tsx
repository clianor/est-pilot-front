import React, { ChangeEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import authModule from '../../../store/moduels/auth';
import { AuthLayout } from '../../../layouts/auth/AuthLayout';
import { RootState } from '../../../store';
import useRegister, { useRegisterProps } from '../../../hooks/auth/useRegister';

function RegisterForm() {
  const history = useHistory();
  const dispatch = useDispatch();
  const authState = useSelector((state: RootState) => state.auth);
  const [form, setForm] = useState<useRegisterProps>({
    email: '',
    password: '',
    passwordConfirm: '',
  });
  const { onRegister } = useRegister(form);

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  useEffect(() => {
    dispatch(authModule.actions.initAction());
  }, []);

  useEffect(() => {
    if (authState.data?.success && !authState?.user) {
      history.push({
        pathname: '/login',
        state: { prev: '/register' },
      });
    } else if (authState.data?.success && authState?.user) {
      history.replace('/');
    }
  }, [authState.data?.success]);

  return (
    <AuthLayout
      type="register"
      form={form}
      isLoading={authState.loading}
      errMsg={authState.errMsgs && authState.errMsgs[0]}
      onSubmit={onRegister}
      onChange={onChange}
    />
  );
}

export default RegisterForm;
