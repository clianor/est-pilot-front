import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import io from 'socket.io-client';

import clientModule from '../store/moduels/client';

export const socket = io(process.env.REACT_APP_SERVER_URL as string);

const useSocketIO = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    // socketio의 세션 초기화를 위함
    socket.disconnect();
    socket.connect();

    socket.emit('join', 'users');

    socket.on('connect', () => {
      socket.emit('join', 'users');
    });

    socket.on('connect_error', () => {
      setTimeout(() => {
        socket.connect();
      }, 5000);
    });

    socket.on('disconnect', () => {
      socket.emit('join', 'users');
    });

    socket.on('returnedError', (data: any) => {
      console.error('returnedError', data);
    });

    socket.on('clientList', (data: any) => {
      dispatch(
        clientModule.actions.client_list({ clientList: data.clientList || [] }),
      );
    });

    socket.on('returnedGetDirectory', (_: string, data: any) => {
      dispatch(clientModule.actions.set_directory_list(data));
    });

    socket.on('returnedRenameClient', (_: string, data: any) => {
      if (data.success) {
        dispatch(clientModule.actions.set_directory_list(data));
      }

      alert(
        data.success
          ? '이름 변경에 성공하였습니다.'
          : `이름 변경에 실패하였습니다.\r\n${data.error}`,
      );
    });

    socket.on('returnedCopyClient', (_: string, data: any) => {
      if (data.success) {
        dispatch(clientModule.actions.set_directory_list(data));
      }

      alert(
        data.success
          ? '복사에 성공하였습니다.'
          : `복사에 실패하였습니다.\r\n${data.error}`,
      );
    });

    socket.on('returnedDeleteClient', (_: string, data: any) => {
      if (data.success) {
        dispatch(clientModule.actions.set_directory_list(data));
      }

      alert(
        data.success
          ? '삭제에 성공하였습니다.'
          : `삭제에 실패하였습니다.\r\n${data.error}`,
      );
    });

    socket.on('returnedMoveClient', (_: string, data: any) => {
      if (data.success) {
        dispatch(clientModule.actions.set_directory_list(data));
      }

      alert(
        data.success
          ? '이동에 성공하였습니다.'
          : `이동에 실패하였습니다.\r\n${data.error}`,
      );
    });

    socket.on('finishUploadFile', (_: string, data: any) => {
      if (data.success) {
        dispatch(
          clientModule.actions.set_directory_list({
            directory: data.directory,
            loading: false,
          }),
        );
      }

      alert(
        data.success
          ? '파일 업로드에 성공하였습니다.'
          : `파일 업로드에 실패하였습니다.\r\n${data.error}`,
      );
    });

    return () => {
      // 이벤트 중복을 피하기 위함.
      socket.off('hello');
      socket.off('clientList');
      socket.off('returnedGetDirectory');
      socket.off('returnedRenameClient');
      socket.off('returnedCopyClient');
      socket.off('returnedDeleteClient');
      socket.off('returnedMoveClient');
      socket.off('finishUploadFile');
    };
  }, []);
};

export default useSocketIO;
