import { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { socket } from '../useSocketIO';
import { RootState } from '../../store';

const useGetDirectory = () => {
  const clientState = useSelector((state: RootState) => state.client);

  const reqDirectoryEvent = useCallback(() => {
    socket.emit('getDirectory', clientState.client, { path: clientState.path });
  }, [clientState]);

  return { reqDirectoryEvent };
};

export default useGetDirectory;
