/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from '@emotion/react';
import React, { FormEvent, useEffect, useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import path from 'path';

import InputBox from '../../components/Input/InputBox';
import Button from '../../components/Button';
import { socket } from '../useSocketIO';

const useMoveForDir = (clientId: string, selection: any) => {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState('');

  useEffect(() => {
    setValue(selection[0]?.path);
  }, [selection]);

  const onMoveClick = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    socket.emit('moveClient', clientId, {
      fromPath: selection[0]?.path,
      toPath: path.join(value),
    });
    handleClose();
  };

  return {
    moveDialog: (
      <Dialog onClose={handleClose} aria-labelledby="dialog-title" open={open}>
        <DialogTitle id="dialog-title">이동</DialogTitle>
        <form
          onSubmit={onSubmit}
          css={css`
            min-width: 400px;
          `}
        >
          <InputBox value={selection[0]?.path} disabled />
          <InputBox value={value} onChange={e => setValue(e.target.value)} />
          <Button
            css={css`
              width: 100%;
            `}
          >
            이동
          </Button>
        </form>
      </Dialog>
    ),
    onMoveClick,
  };
};

export default useMoveForDir;
