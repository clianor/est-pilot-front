/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from '@emotion/react';
import React, { FormEvent, useEffect, useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import path from 'path';

import InputBox from '../../components/Input/InputBox';
import Button from '../../components/Button';
import { socket } from '../useSocketIO';

const useRenameForDir = (clientId: string, selection: any) => {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState('');

  useEffect(() => {
    setValue(selection[0]?.name);
  }, [selection]);

  const onRenameClick = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    socket.emit('renameClient', clientId, {
      fromPath: selection[0]?.path,
      toPath: path.join(selection[0]?.dir, value),
    });
    handleClose();
  };

  return {
    renameDialog: (
      <Dialog onClose={handleClose} aria-labelledby="dialog-title" open={open}>
        <DialogTitle id="dialog-title">이름 변경</DialogTitle>
        <form onSubmit={onSubmit}>
          <InputBox value={value} onChange={e => setValue(e.target.value)} />
          <Button
            css={css`
              width: 100%;
            `}
          >
            변경
          </Button>
        </form>
      </Dialog>
    ),
    onRenameClick,
  };
};

export default useRenameForDir;
