import { useCallback, useEffect, useState } from 'react';
import { pathSplit } from '../../shared/pathSplit';

import { socket } from '../useSocketIO';

export interface IFolder {
  name: string;
  path: string;
  sepPath: string[];
  children: IFolder[];
}

const useReqFolders = (isActiveMount = true) => {
  const [folders, setFolders] = useState<IFolder[]>([]);

  const reqFoldersEvent = useCallback((clientId: string, path = '/') => {
    socket.emit('reqFolders', clientId, { path, isFolder: true });
  }, []);

  const removeFolderChildren = useCallback(
    (path: string) => {
      const visitor = getVisitor(folders, path.split(/[\\|/]/));

      const visitorEnd = visitor.pop() as number;
      let tmpFolders = folders;
      for (const idx of visitor) {
        tmpFolders = tmpFolders[idx].children;
      }
      tmpFolders[visitorEnd].children = [];
      setFolders([...folders]);
    },
    [folders],
  );

  useEffect(() => {
    if (isActiveMount) {
      socket.on('returnedReqFolders', (_: string, data: any) => {
        if (data.success) {
          if (data.path === '/') {
            setFolders([
              {
                name: '/',
                path: '/',
                sepPath: ['/'],
                children: data.directory.map(
                  (item: { name: string; path: string; sepPath: string[] }) => {
                    return {
                      name: item.name,
                      path: item.path,
                      sepPath: item.sepPath,
                      children: [],
                    };
                  },
                ),
              },
            ]);
          } else {
            const directory: IFolder[] = data.directory
              .splice(1)
              .map(
                (item: { name: string; path: string; sepPath: string[] }) => {
                  return {
                    name: item.name,
                    path: item.path,
                    sepPath: item.sepPath,
                    children: [],
                  };
                },
              );

            setFolders([
              ...assignFolder(folders, directory, pathSplit(data.path)),
            ]);
          }
        }
      });

      return () => {
        socket.off('returnedReqFolders');
      };
    }
  }, [folders]);

  return { reqFoldersEvent, removeFolderChildren, folders };
};

function assignFolder(
  folders: IFolder[],
  directory: IFolder[],
  splitPath?: string[],
) {
  if (!splitPath) {
    return folders;
  }

  const visitor = getVisitor(folders, splitPath);

  const visitorEnd = visitor.pop() as number;
  let tmpFolders = folders;
  for (const idx of visitor) {
    tmpFolders = tmpFolders[idx].children;
  }
  tmpFolders[visitorEnd].children = directory;

  return folders;
}

function getVisitor(folders: IFolder[], splitPath: string[]): number[] {
  const visitor: number[] = [0];

  for (const split of splitPath) {
    let tmpFolder = folders;
    for (const idx of visitor) {
      tmpFolder = tmpFolder[idx].children;
      tmpFolder.filter((item, idx) => {
        const isCompare =
          item.name.replace(/[\\|/]/, '') === split.replace(/[\\|/]/, '');
        if (isCompare) visitor.push(idx);
        return isCompare;
      });
    }
  }

  return visitor;
}

export default useReqFolders;
