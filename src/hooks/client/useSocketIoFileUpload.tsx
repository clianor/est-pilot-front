import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import clientModule from '../../store/moduels/client';
import { RootState } from '../../store';
import { socket } from '../useSocketIO';
import usePreventLeave from '../usePreventLeave';

const useSocketIoFileUpload = () => {
  const dispatch = useDispatch();
  const loading = useSelector((state: RootState) => state.client.loading);
  const file = useSelector((state: RootState) => state.client.file);
  const { enablePrevent, disablePrevent } = usePreventLeave();

  useEffect(() => {
    socket.on('reqUploadFile', (clientId: string, data: any) => {
      const { chunkSize, chunk, chunks } = data;
      if (file && chunkSize > chunk) {
        const fileReader = new FileReader();
        fileReader.readAsArrayBuffer(file);
        fileReader.onload = () => {
          const start = chunkSize * chunk;
          const end = chunkSize * chunk + chunkSize;
          data['chunk'] = chunk + 1;
          data['data'] = fileReader.result?.slice(start, end);
          socket.emit('reqUploadFile', clientId, data);
        };
      }

      dispatch(clientModule.actions.set_file_chunks({ chunk, chunks }));
    });

    return () => {
      socket.off('reqUploadFile');
    };
  }, [file]);

  useEffect(() => {
    if (loading) enablePrevent();

    return () => {
      if (loading) disablePrevent();
    };
  }, [loading]);
};

export default useSocketIoFileUpload;
