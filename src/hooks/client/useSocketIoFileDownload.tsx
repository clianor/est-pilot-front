import { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { socket } from '../useSocketIO';
import { RootState } from '../../store';
import clientActions from '../../store/moduels/client';

let fileArray: Uint8Array = new Uint8Array(0);

const useSocketIoFileDownload = () => {
  const dispatch = useDispatch();
  const { client } = useSelector((state: RootState) => state.client);

  const ref = useRef(null);

  const donwloadHandler = (path: string, name: string) => {
    socket.emit('startDownloadFile', client, { path, name });
    dispatch(clientActions.actions.set_loading(true));
  };

  useEffect(() => {
    socket.on('reqDownloadFile', (_: string, data: any) => {
      const array = new Uint8Array(data.data);

      if (array) {
        const tmpArray = new Uint8Array(
          fileArray.byteLength + array.byteLength,
        );
        tmpArray.set(fileArray, 0);
        tmpArray.set(array, fileArray.byteLength);
        fileArray = tmpArray;
      }
    });

    socket.on('finishDownloadFile', (_: string, data: any) => {
      if (data.success) {
        const arrayBuffer = fileArray.buffer;
        const blob = new Blob([arrayBuffer]);
        const url = window.URL.createObjectURL(blob);
        if (ref !== null) {
          const currentTarget = (ref as any).current;
          currentTarget.href = url;
          currentTarget.download = data.name;
          currentTarget.click();
          window.URL.revokeObjectURL(url);
        }
      }
      fileArray = new Uint8Array(0);
      dispatch(clientActions.actions.set_loading(false));
    });

    return () => {
      socket.off('reqDownloadFile');
      socket.off('finishDownloadFile');
    };
  }, [ref]);

  return {
    donwloadHandler,
    ref,
  };
};

export default useSocketIoFileDownload;
