/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from '@emotion/react';
import React, { FormEvent, useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

import Button from '../../components/Button';
import { socket } from '../useSocketIO';

const useDeleteClient = (clientId: string, selection: any) => {
  const [open, setOpen] = useState(false);

  const onDeleteClick = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    socket.emit('deleteClient', clientId, { fromPath: selection[0]?.path });
    handleClose();
  };

  return {
    deleteDialog: (
      <Dialog onClose={handleClose} aria-labelledby="dialog-title" open={open}>
        <DialogTitle id="dialog-title">삭제</DialogTitle>
        <form
          onSubmit={onSubmit}
          css={css`
            min-width: 200px;
          `}
        >
          <p
            css={css`
              padding: 0 10px;
            `}
          >
            삭제하시겠습니까?
          </p>
          <Button
            css={css`
              width: 100%;
            `}
          >
            삭제
          </Button>
        </form>
      </Dialog>
    ),
    onDeleteClick,
  };
};

export default useDeleteClient;
