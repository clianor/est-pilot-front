import { FormEvent } from 'react';
import { useDispatch } from 'react-redux';

import authModule from '../../store/moduels/auth';

export interface useLoginProps {
  email: string;
  password: string;
}

const useLogin = (form: useLoginProps) => {
  const dispatch = useDispatch();

  const onLogin = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    dispatch(
      authModule.actions.loginActions.request({ loading: true, ...form }),
    );
  };

  return { onLogin };
};

export default useLogin;
