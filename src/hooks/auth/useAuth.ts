import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import authModule from '../../store/moduels/auth';

const useAuth = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      authModule.actions.meActions.request({ loading: true, user: undefined }),
    );
  }, []);
};

export default useAuth;
