import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '../../store';
import authModule from '../../store/moduels/auth';
import clientModule from '../../store/moduels/client';

const useLogout = () => {
  const dispatch = useDispatch();
  const { user } = useSelector((state: RootState) => state.auth);

  const onLogout = () => {
    dispatch(authModule.actions.logoutActions.request({ loading: true }));
  };

  useEffect(() => {
    if (!user) {
      dispatch(clientModule.actions.clear());
    }
  }, [user]);

  return { onLogout };
};

export default useLogout;
