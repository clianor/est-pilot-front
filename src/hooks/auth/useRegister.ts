import { FormEvent } from 'react';
import { useDispatch } from 'react-redux';

import authModule from '../../store/moduels/auth';

export interface useRegisterProps {
  email: string;
  password: string;
  passwordConfirm: string;
}

const useRegister = (form: useRegisterProps) => {
  const dispatch = useDispatch();

  const onRegister = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    dispatch(
      authModule.actions.registerActions.request({ loading: true, ...form }),
    );
  };

  return { onRegister };
};

export default useRegister;
