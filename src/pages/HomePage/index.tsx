/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from '@emotion/react';

import LeftBox from './LeftBox';
import Content from '../../components/Layout/Content';
import RightBox from './RightBox';
import useSocketIO from '../../hooks/useSocketIO';

function HomePage() {
  useSocketIO();

  return (
    <Content css={HomePageStyle}>
      <LeftBox />
      <RightBox />
    </Content>
  );
}

const HomePageStyle = css`
  display: flex;
`;

export default HomePage;
