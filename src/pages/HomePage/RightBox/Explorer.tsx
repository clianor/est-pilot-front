import em from '@emotion/styled';
import React, { MouseEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  DataGrid,
  GridOverlay,
  RowSelectedParams,
  SortModel,
} from '@material-ui/data-grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFolder, faFileAlt } from '@fortawesome/free-solid-svg-icons';

import clientModule from '../../../store/moduels/client';
import ExplorerHeader from './ExplorerHeader';
import { RootState } from '../../../store';
import useSocketIoFileDownload from '../../../hooks/client/useSocketIoFileDownload';
import useGetDirectory from '../../../hooks/client/useGetDirectory';
import useInterval from '../../../hooks/useInterval';
import { LinearProgress } from '@material-ui/core';

function Explorer() {
  const dispatch = useDispatch();
  const clientState = useSelector((state: RootState) => state.client);
  const [selection, setSelection] = useState<any[]>([]);

  const [sortModel, setSortModel] = useState([]);
  const [rows, setRows] = useState<any>([]);
  const [loading, setLoading] = useState(false);

  const { reqDirectoryEvent } = useGetDirectory();

  useInterval(() => {
    reqDirectoryEvent();
  }, 5000);

  const { donwloadHandler, ref } = useSocketIoFileDownload();

  const FileNameInRowComponent = (params: any) => {
    return (
      <strong
        onDoubleClick={onDoubleClick}
        data-path={params.row.path}
        data-isfolder={params.row.isFolder}
        data-isfile={params.row.isFile}
        style={{
          width: '100%',
          textAlign: 'left',
          userSelect: 'none',
          cursor: 'pointer',
        }}
        title={params.value}
      >
        <FontAwesomeIcon icon={params.row.isFolder ? faFolder : faFileAlt} />
        &emsp;
        {params.value}
      </strong>
    );
  };

  const columns = [
    {
      field: 'name',
      headerName: '이름',
      width: 300,
      renderCell: FileNameInRowComponent,
    },
    { field: 'ext', headerName: '유형', width: 150 },
    { field: 'mtime', headerName: '수정한 날짜', width: 300 },
    { field: 'size', headerName: '크기', width: 150 },
  ];

  const onDoubleClick = (event: MouseEvent<HTMLLIElement>) => {
    const { path, isfolder, isfile } = event.currentTarget.dataset;

    setSelection([]);
    if (isfolder === 'true') {
      dispatch(clientModule.actions.set_path({ path }));
    } else if (isfile === 'true') {
      const selectedRow = selection[0];
      donwloadHandler(selectedRow.path, selectedRow.name);
    }
  };

  const handleSortModelChange = (params: any) => {
    if (params.sortModel !== sortModel) {
      setSortModel(params.sortModel);
    }
  };

  useEffect(() => {
    let active = true;

    (async () => {
      setLoading(true);
      const newRows = await loadRows(sortModel, clientState);

      if (!active) {
        return;
      }

      setRows(newRows);
      setLoading(false);
    })();

    return () => {
      active = false;
    };
  }, [sortModel, clientState.directory]);

  return (
    <>
      <StyledHideAnchor ref={ref} />
      <StyledGridWrapper>
        <ExplorerHeader selection={selection} client={clientState.client} />
        <DataGrid
          rows={rows}
          columns={columns}
          onRowSelected={(param: RowSelectedParams) => {
            param.data.name === '...' || param.data.name === param.data.path
              ? setSelection([])
              : setSelection([param.data]);
          }}
          components={{
            loadingOverlay: CustomLoadingOverlay,
          }}
          sortingMode="server"
          sortModel={sortModel}
          onSortModelChange={handleSortModelChange}
          loading={loading || clientState.loading}
        />
      </StyledGridWrapper>
    </>
  );
}

const StyledGridWrapper = em.div`
height: calc(100vh - 190px);
`;

const StyledHideAnchor = em.a`
display: none;
`;
function CustomLoadingOverlay() {
  const chunk = useSelector((state: RootState) => state.client.chunk) as number;
  const chunks = useSelector(
    (state: RootState) => state.client.chunks,
  ) as number;

  return (
    <GridOverlay>
      <div style={{ position: 'absolute', top: 0, width: '100%' }}>
        <LinearProgress variant="determinate" value={(chunk / chunks) * 100} />
      </div>
    </GridOverlay>
  );
}

function loadRows(sortModel: SortModel, clientState: any) {
  return new Promise<any>(resolve => {
    const rows = clientState.directory;

    if (sortModel?.length === 0 || rows?.length === 0) {
      resolve(
        clientState.directory.map((row: any, index: number) => {
          row['id'] = index;
          return row;
        }),
      );
      return;
    }

    const sortedColumn = sortModel[0];
    const firstRow = rows[0];
    const isParentRow = isMoveParentRow(firstRow);

    let sortedRows = [
      ...(isParentRow ? rows.slice(1, rows.length) : rows).sort(
        (a: any, b: any) => {
          return String(a[sortedColumn.field]).localeCompare(
            String(b[sortedColumn.field]),
          );
        },
      ),
    ];

    if (sortedColumn.sort === 'desc') {
      sortedRows = [...sortedRows].reverse();
    }

    if (isParentRow) {
      sortedRows = [firstRow, ...sortedRows];
    }

    sortedRows = [
      ...sortedRows.map((row: any, index: number) => {
        row['id'] = index;
        return row;
      }),
    ];

    resolve(sortedRows);
  });
}

function isMoveParentRow(row: {
  name: string;
  id: number;
  isFolder: boolean;
}): boolean {
  const { name, isFolder } = row;
  return name === '...' && isFolder === true ? true : false;
}

export default Explorer;
