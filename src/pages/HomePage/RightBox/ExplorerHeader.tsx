import em from '@emotion/styled';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Button from '../../../components/Button';
import FileUploadInput from '../../../components/Input/FileUploadInput';
import useRenameClient from '../../../hooks/client/useRenameClient';
import useCopyClient from '../../../hooks/client/useCopyClient';
import useDeleteClient from '../../../hooks/client/useDeleteClient';
import useMoveClient from '../../../hooks/client/useMoveClient';
import { socket } from '../../../hooks/useSocketIO';
import { RootState } from '../../../store';
import clientModule from '../../../store/moduels/client';
import useSocketIoFileUpload from '../../../hooks/client/useSocketIoFileUpload';

interface ExplorerHeaderProps {
  selection: object[];
  client: string;
}

function ExplorerHeader({ selection }: ExplorerHeaderProps) {
  const dispatch = useDispatch();
  const clientState = useSelector((state: RootState) => state.client);
  useSocketIoFileUpload();

  const { renameDialog, onRenameClick } = useRenameClient(
    clientState.client,
    selection,
  );
  const { copyDialog, onCopyClick } = useCopyClient(
    clientState.client,
    selection,
  );
  const { deleteDialog, onDeleteClick } = useDeleteClient(
    clientState.client,
    selection,
  );
  const { moveDialog, onMoveClick } = useMoveClient(
    clientState.client,
    selection,
  );

  const onChange = (event: any) => {
    const file = event.target.files[0];
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    const chunkSize =
      parseInt(process.env.REACT_APP_FILE_CHUNK_LIMIT as string) | 512000;
    const chunks = Math.ceil(file.size / chunkSize);
    const chunk = 0;

    socket.emit('startUploadFile', clientState.client, {
      path: clientState.path,
      name: file.name,
      chunkSize,
      chunks,
      chunk,
    });

    dispatch(clientModule.actions.set_file({ file, chunk, chunks }));
  };

  return (
    <StyledExplorerHeader>
      {selection.length !== 0 && (
        <StyledLeftWrapper>
          <Button size="small" onClick={onCopyClick}>
            복사
          </Button>
          <Button size="small" onClick={onMoveClick}>
            이동
          </Button>
          <Button size="small" onClick={onDeleteClick}>
            삭제
          </Button>
          <Button size="small" onClick={onRenameClick}>
            이름 변경
          </Button>
          {renameDialog}
          {copyDialog}
          {deleteDialog}
          {moveDialog}
        </StyledLeftWrapper>
      )}

      <StyledRightWrapper>
        <FileUploadInput id={'upload'} onChange={onChange} value="" />
      </StyledRightWrapper>
    </StyledExplorerHeader>
  );
}

const StyledExplorerHeader = em.div`
  height: 50px;
  padding: 0 30px;
`;

const StyledLeftWrapper = em.span`
  float: left;
`;

const StyledRightWrapper = em.span`
  float: right;
`;

export default ExplorerHeader;
