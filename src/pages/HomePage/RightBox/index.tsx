import em from '@emotion/styled';
import { faFolder } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { useSelector } from 'react-redux';

import Explorer from './Explorer';
import { RootState } from '../../../store';
import Button from '../../../components/Button';
import useLogout from '../../../hooks/auth/useLogout';

function RightBox() {
  const clientState = useSelector((state: RootState) => state.client);
  const authState = useSelector((state: RootState) => state.auth);
  const { onLogout } = useLogout();

  return (
    <StyledRightBox>
      <StyledHeader>
        <div className="header__left">
          <FontAwesomeIcon icon={faFolder} />
          <div className="header__path">
            <h1>{clientState.client && clientState.path}</h1>
          </div>
        </div>
        <div className="header__right">
          <Button
            size="small"
            onClick={onLogout}
            isLoading={authState.logoutLoading}
          >
            로그아웃
          </Button>
        </div>
      </StyledHeader>
      <Explorer />
    </StyledRightBox>
  );
}

const StyledHeader = em.header`
  display: flex;
  justify-content: space-between;
  text-align: left;
  padding: 10px 30px;

  & > .header__left {
    display: flex;
    flex: 1;

    & > svg {
      color: #fcd462;
      font-size: 120px;
    }
  
    & > .header__path {
      padding: 10px 30px;
  
      & > h1 {
        font-size: 45px;
        margin: 0;
      }
    }
  }
`;

const StyledRightBox = em.div`
  flex: 1;
`;

export default RightBox;
