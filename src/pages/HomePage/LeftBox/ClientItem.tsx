import em from '@emotion/styled';
import React, { MouseEvent } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

import { IFolder } from '../../../hooks/client/useReqFolders';
import TreeView from './TreeView';

interface ClientItemProps {
  isActive?: boolean;
  ip: string;
  clientId: string;
  folders: IFolder[];
  path?: string;
  onClick(e: MouseEvent<HTMLLIElement>): void;
  reqFoldersEvent(clientId: string, path?: string): void;
  removeFolderChildren(path: string): void;
  [k: string]: any;
}

function ClientItem({
  ip,
  clientId,
  isActive = false,
  folders,
  reqFoldersEvent,
  removeFolderChildren,

  ...rest
}: ClientItemProps) {
  return (
    <>
      <StyledItem
        className={isActive ? 'active' : ''}
        data-id={clientId}
        {...rest}
      >
        <FontAwesomeIcon icon={isActive ? faEye : faEyeSlash} />
        {ip}
      </StyledItem>
      {isActive && (
        <TreeView
          clientId={clientId}
          folders={folders}
          reqFoldersEvent={reqFoldersEvent}
          removeFolderChildren={removeFolderChildren}
        />
      )}
    </>
  );
}

const StyledItem = em.li`
  cursor: pointer;
  list-style-type: none;
  display: flex;
  align-items: center;
  padding: 15px 30px;
  text-align: left;
  overflow: hidden;

  & > svg {
    margin-right: 16px;
  }

  &:hover, & > &.active {
    background-color: #6574c4;
  }
`;

export default React.memo(ClientItem);
