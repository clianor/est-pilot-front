import em from '@emotion/styled';
import React, { MouseEvent } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretRight, faCaretDown } from '@fortawesome/free-solid-svg-icons';

import { IFolder } from '../../../hooks/client/useReqFolders';

interface ClientItemTreeItemProps {
  key?: string;
  clientId: string;
  folders: IFolder[];
  depth?: number;
  onClick?(e: MouseEvent<HTMLDivElement>): void;
  onRemoveClick?(e: MouseEvent<HTMLDivElement>): void;
}

function ClientItemTreeItem({
  clientId,
  folders,
  onClick,
  onRemoveClick,
  depth = 0,
}: ClientItemTreeItemProps) {
  return (
    <StyledTreeItem>
      {folders.map(folder => {
        return (
          <React.Fragment key={folder.path}>
            {folders.length !== 0 && folders[0].name !== '/' && (
              <TreeItem
                folder={folder}
                depth={depth}
                onClick={onClick}
                onRemoveClick={onRemoveClick}
              />
            )}
            <ClientItemTreeItem
              clientId={clientId}
              folders={folder.children || []}
              depth={depth + 1}
              onClick={onClick}
              onRemoveClick={onRemoveClick}
            />
          </React.Fragment>
        );
      })}
    </StyledTreeItem>
  );
}

interface TreeItemProps {
  folder: IFolder;
  depth: number;
  onClick?(e: MouseEvent<HTMLDivElement>): void;
  onRemoveClick?(e: MouseEvent<HTMLDivElement>): void;
}

function TreeItem({ folder, depth, onClick, onRemoveClick }: TreeItemProps) {
  return (
    <StyledHoverDiv onClick={onClick} data-path={folder.path}>
      {'\u00A0'.repeat(depth * 3 - 3)}
      <span className="fa__Caret" onClick={onRemoveClick}>
        <FontAwesomeIcon
          icon={folder.children.length === 0 ? faCaretRight : faCaretDown}
        />
      </span>
      {'\u00A0'}
      {folder.name}
    </StyledHoverDiv>
  );
}

const StyledTreeItem = em.div`
  text-align: left;
  font-size: 14px;
`;

const StyledHoverDiv = em.div`
  cursor: pointer;
  padding: 10px 0;
  padding-left: 30px;

  &:hover {
    background-color: #6574c4;
  }

  & > .fa__Caret {
    padding-right: 10px;
  }

  & > .fa__Caret:hover {
    color: blue;
  }
`;

export default ClientItemTreeItem;
