import em from '@emotion/styled';
import React, { MouseEvent } from 'react';
import { useDispatch } from 'react-redux';

import ClientItem from './ClientItem';
import clientModule from '../../../store/moduels/client';
import useReqFolders from '../../../hooks/client/useReqFolders';

interface ClientItemContainerProps {
  client?: string;
  clientList: string[];
}

function ClientItemContainer({ client, clientList }: ClientItemContainerProps) {
  const dispatch = useDispatch();
  const { reqFoldersEvent, removeFolderChildren, folders } = useReqFolders();

  const onClick = (event: MouseEvent<HTMLLIElement>) => {
    const clientId = event.currentTarget.dataset?.id as string;

    if (clientId !== client) {
      dispatch(
        clientModule.actions.set_client({ client: clientId, path: '/' }),
      );
      reqFoldersEvent(clientId);
    }
  };

  return (
    <StyledClientList>
      {clientList.map((item: any) => (
        <ClientItem
          key={item[0]}
          clientId={item[0]}
          ip={item[1]}
          onClick={onClick}
          isActive={client === item[0]}
          folders={client === item[0] ? folders : []}
          reqFoldersEvent={reqFoldersEvent}
          removeFolderChildren={removeFolderChildren}
        />
      ))}
    </StyledClientList>
  );
}

const StyledClientList = em.ul`
  margin: 0;
  padding: 0;
  overflow: hidden;
  color: #ffffff;
  font-size: 20px;
  height: calc(100vh - 102px);
  overflow-y: auto;
`;

export default ClientItemContainer;
