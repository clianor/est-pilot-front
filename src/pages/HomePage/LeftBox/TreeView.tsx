import React, { MouseEvent } from 'react';
import { useDispatch } from 'react-redux';

import clientModule from '../../../store/moduels/client';
import { IFolder } from '../../../hooks/client/useReqFolders';
import TreeViewItem from './TreeViewItem';

interface ClientItemTreeContainerProps {
  clientId: string;
  folders: IFolder[];
  reqFoldersEvent(clientId: string, path?: string): void;
  removeFolderChildren(path: string): void;
}

function ClientItemTreeContainer({
  clientId,
  folders,
  reqFoldersEvent,
  removeFolderChildren,
}: ClientItemTreeContainerProps) {
  const dispatch = useDispatch();

  const onClick = (e: MouseEvent<HTMLDivElement>) => {
    const path = e.currentTarget.dataset?.path;
    reqFoldersEvent(clientId, path);
    dispatch(clientModule.actions.set_path({ path }));
  };

  const onRemoveClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    const path = e.currentTarget.parentElement?.dataset.path;
    if (path) {
      removeFolderChildren(path);
    }
  };

  return (
    <TreeViewItem
      clientId={clientId}
      folders={folders}
      onClick={onClick}
      onRemoveClick={onRemoveClick}
    />
  );
}

export default ClientItemTreeContainer;
