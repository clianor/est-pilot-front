import em from '@emotion/styled';
import React from 'react';
import { useSelector } from 'react-redux';

import { RootState } from '../../../store';
import ClientItemContainer from './ClientItemContainer';

function LeftBox() {
  // 리렌더 방지를 위해 리덕스 스토어에서 각각의 값을 단일 객체로 가져옴
  // 현재 이 방법이 가장 유용한 방법이라고 소개하고 있음
  // https://9oelm.github.io/2020-09-13--How-to-make-useSelector-not-a-disaster/
  const client = useSelector((state: RootState) => state.client.client);
  const clientList = useSelector((state: RootState) => state.client.clientList);

  return (
    <StyledLeftBox>
      <h1 className="box__header">클라이언트</h1>
      <ClientItemContainer client={client} clientList={clientList} />
    </StyledLeftBox>
  );
}

const StyledLeftBox = em.div`
  width: 320px;
  background-color: #3F51B5;

  .box__header {
    font-size: 40px;
    color: #ffffff;
    margin: 0;
    line-height: 100px;
    border-bottom: 2px solid #ffffff;
    user-select: none;
  }
`;

export default LeftBox;
