/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from '@emotion/react';
import em from '@emotion/styled';
import React from 'react';

import Content from '../../components/Layout/Content';
import LoginForm from '../../containers/auth/LoginForm';

function LoginPage() {
  return (
    <Content css={LoginPageStyle}>
      <StyledLeftBox />
      <StyledRightBox>
        <LoginForm />
      </StyledRightBox>
    </Content>
  );
}

const LoginPageStyle = css`
  display: flex;
`;

const StyledLeftBox = em.div`
  flex: 2;
  background-color: #3F51B5;

  @media (max-width: 960px) {
    display: none;
  }
`;

const StyledRightBox = em.div`
  flex: 5;
  display: flex;
  align-items: center;
  background-color: #9FA8DA;
`;

export default LoginPage;
