/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from '@emotion/react';
import em from '@emotion/styled';
import React from 'react';

import Content from '../../components/Layout/Content';
import RegisterForm from '../../containers/auth/RegisterForm';

function RegisterPage() {
  return (
    <Content css={RegisterPageStyle}>
      <StyledLeftBox />
      <StyledRightBox>
        <RegisterForm />
      </StyledRightBox>
    </Content>
  );
}

const RegisterPageStyle = css`
  display: flex;
`;

const StyledLeftBox = em.div`
  flex: 2;
  background-color: #3F51B5;

  @media (max-width: 960px) {
    display: none;
  }
`;

const StyledRightBox = em.div`
  flex: 5;
  display: flex;
  align-items: center;
  background-color: #9FA8DA;
`;

export default RegisterPage;
