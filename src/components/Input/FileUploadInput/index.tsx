import em from '@emotion/styled';
import React from 'react';

interface FileUploadInputProps {
  id: string;
  onChange?: any;
  value?: any;
}

const FileUploadInput = ({ id, onChange, value }: FileUploadInputProps) => {
  return (
    <Wrapper>
      <label htmlFor={id}>업로드</label>
      <input type="file" id={id} onChange={onChange} value={value} />
    </Wrapper>
  );
};

const Wrapper = em.div`
  display: flex;

  & > input[type="file"] {
    display: none;
  }

  & > label {
    outline: none;
    cursor: pointer;
    background-color: #2185d0;
    color: #fff;
    border-radius: 0.25rem;
    box-sizing: border-box;
    padding: 0.75rem 1rem;
    line-height: 1rem;
    font-size: 0.875rem;
    font-weight: 600;
    word-break: keep-all;
  }
`;

export default FileUploadInput;
