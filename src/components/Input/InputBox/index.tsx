import em from '@emotion/styled';
import React, { Fragment, InputHTMLAttributes } from 'react';

type InputBoxProps = InputHTMLAttributes<HTMLInputElement> & {
  labelText?: string;
};

const InputBox = ({ labelText, ...rest }: InputBoxProps) => {
  return (
    <Fragment>
      {labelText && <label htmlFor={rest['id']}>{labelText}</label>}
      <StyledInputBox {...rest} />
    </Fragment>
  );
};

const StyledInputBox = em.input`
  display: block;
  width: 100%;
  height: 100%;
  border: 1px solid #E6E9F4;
  box-sizing: border-box;
  outline: none;
  padding: 0.75rem 1rem;
  font-family: inherit;
  font-size: inherit;
`;

export default InputBox;
