import React, { useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, useHistory } from 'react-router-dom';

import useAuth from '../../hooks/auth/useAuth';
import { RootState } from '../../store';

interface PrivateRouteProps {
  children: React.ReactChild;
  [key: string]: any;
}

/**
 *
 * @param {boolean} isAuth  true: 로그인 유저만 허용, false: 로그인 유저는 접속 불가
 */
const PrivateRoute = ({ children, isAuth, ...rest }: PrivateRouteProps) => {
  useAuth();
  const history = useHistory();
  const { meLoading, user } = useSelector((state: RootState) => state.auth);

  const memoizedReturned = useMemo(() => {
    if (isAuth === true && meLoading === false && user === null) {
      // 로그인 유저가 아니면 로그인페이지로 이동
      return <Redirect to={{ pathname: '/login' }} />;
    } else if (
      (isAuth === true && meLoading === false && user) ||
      (isAuth === false && meLoading === false && user === null)
    ) {
      return <Route {...rest} render={() => children} />;
    } else if (isAuth === false && meLoading === false && !!user) {
      // 비로그인 유저는 접속 불가
      return <Redirect to={{ pathname: '/' }} />;
    }
    return <div>Loading...</div>;
  }, [isAuth, user]);

  useEffect(() => {
    if (isAuth === true) {
      // 로그인 유저만 허용
      if (!user) {
        history.push('/login');
      }
    } else if (isAuth === false) {
      // 로그인 유저는 비허용
      if (user) history.push('/');
    }
  }, [user]);

  return memoizedReturned;
};

export default PrivateRoute;
