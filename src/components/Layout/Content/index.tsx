import em from '@emotion/styled';
import React, { HTMLAttributes } from 'react';

const Content = ({ children, ...rest }: HTMLAttributes<HTMLDivElement>) => {
  return <StyledContent {...rest}>{children}</StyledContent>;
};

const StyledContent = em.main`
  display: block;
  text-align: center;
  height: 100vh;
`;

export default Content;
