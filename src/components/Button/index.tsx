/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, css } from '@emotion/react';
import em from '@emotion/styled';
import React, { ButtonHTMLAttributes, ReactNode } from 'react';

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  theme?: 'primary' | 'secondary' | 'tertiary';
  size?: 'small' | 'middle' | 'big';
  isLoading?: boolean;
  children: ReactNode;
};

const Button = ({
  children,
  theme = 'primary',
  size = 'small',
  isLoading = false,
  onClick,
  ...rest
}: ButtonProps) => {
  return (
    <StyledButton
      css={[themes[theme], sizes[size]]}
      disabled={isLoading}
      onClick={onClick}
      {...rest}
    >
      {children}
    </StyledButton>
  );
};

const StyledButton = em.button`
  outline: none;
  border-radius: 0.25rem;
  box-sizing: border-box;
  padding: 0.75rem 1rem;
  line-height: 1rem;
  font-weight: 600;
  background-color: #fff;
  cursor: pointer;
  word-break: keep-all;

  &:focus {
    outline: solid
    box-shadow: 0px 0px 8px rgba(87, 184, 255, 0.2);
  }

  &:hover {
    background-color: #0058FF;
    color: #fff;
  }

  &:active {
    background-color: #333;
    color: #fff;
  }
  
  &:disabled, &:disabled:hover, &:disabled:active {
    background-color: lightgray;
    cursor: not-allowed;
  }
`;

const themes = {
  primary: css`
    border: 1px solid #e6e9f4;
    background-color: #2185d0;
    color: #fff;
  `,
  secondary: css`
    border: 1px solid #e6e9f4;
  `,
  tertiary: css`
    border: 1px solid #fff;
  `,
};

const sizes = {
  small: css`
    font-size: 0.875rem;
    line-height: 1rem;
  `,
  middle: css`
    font-size: 1rem;
    line-height: 1.125rem;
  `,
  big: css`
    font-size: 1.125rem;
    line-height: 1.25rem;
  `,
};

export default Button;
