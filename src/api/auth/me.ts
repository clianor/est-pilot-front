import { AxiosRequestConfig } from 'axios';

import axiosInstance from '../../shared/axiosInstance';

export interface IRequest {
  loading: boolean;
  user?: {
    id: string;
    email: string;
  };
}

export interface IResponse {
  user: {
    id: string;
    email: string;
  };
}

export interface IError {
  message: Error;
}

export const fetchMeApi = (): Promise<any> => {
  const options: AxiosRequestConfig = {
    method: 'GET',
    url: '/api/auth/me',
  };

  return axiosInstance(options).then(res => res.data);
};
