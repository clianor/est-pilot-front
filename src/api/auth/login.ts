import { AxiosRequestConfig } from 'axios';
import qs from 'qs';

import axiosInstance from '../../shared/axiosInstance';

export interface IRequest {
  loading: boolean;
}

export interface IResponse {
  data: any[];
}

export interface IError {
  message: Error;
}

export const fetchLoginApi = (data: { [key: string]: any }): Promise<any> => {
  const options: AxiosRequestConfig = {
    method: 'POST',
    url: '/api/auth/login',
    data: qs.stringify(data),
  };

  return axiosInstance(options).then(res => res.data);
};
