import { AxiosRequestConfig } from 'axios';

import axiosInstance from '../../shared/axiosInstance';

export interface IRequest {
  loading: boolean;
}

export interface IResponse {
  data: any[];
}

export interface IError {
  message: Error;
}

export const fetchLogoutApi = (): Promise<any> => {
  const options: AxiosRequestConfig = {
    method: 'GET',
    url: '/api/auth/logout',
  };

  return axiosInstance(options).then(res => res.data);
};
