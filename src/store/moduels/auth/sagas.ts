import { takeLatest } from 'redux-saga/effects';

import { fetchLoginApi } from '../../../api/auth/login';
import { fetchRegisterApi } from '../../../api/auth/register';
import { fetchLogoutApi } from '../../../api/auth/logout';
import { fetchMeApi } from '../../../api/auth/me';
import { createAsyncSaga } from '../../lib';
import actions from './actions';

const sagas = [
  // 로그인 사가
  takeLatest(
    actions.loginActions.request,
    createAsyncSaga(actions.loginActions, fetchLoginApi),
  ),
  // 회원가입 사가
  takeLatest(
    actions.registerActions.request,
    createAsyncSaga(actions.registerActions, fetchRegisterApi),
  ),
  // 로그아웃 사가
  takeLatest(
    actions.logoutActions.request,
    createAsyncSaga(actions.logoutActions, fetchLogoutApi),
  ),
  // 내정보 사가
  takeLatest(
    actions.meActions.request,
    createAsyncSaga(actions.meActions, fetchMeApi),
  ),
];

export default sagas;
