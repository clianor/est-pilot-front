import { Reducer } from 'redux';
import { createReducer } from 'typesafe-actions';

import actions from './actions';

interface IAuthState {
  loading: boolean;
  logoutLoading: boolean;
  meLoading: boolean;
  user?: any;
  data?: any;
  errMsgs?: string[];
}

const initState: IAuthState = {
  loading: false,
  logoutLoading: false,
  meLoading: false,
};

const reducer: Reducer<IAuthState> = createReducer<IAuthState>(initState)
  .handleAction(actions.initAction, (state: IAuthState) => {
    return { ...initState, user: state.user };
  })
  .handleAction(actions.loginActions.request, (state: IAuthState) => {
    // 로그인 요청 액션
    return { ...state, loading: true, errMsgs: undefined };
  })
  .handleAction(
    actions.loginActions.success,
    (state: IAuthState, action: any) => {
      // 로그인 성공 액션
      return {
        ...state,
        loading: false,
        data: action.payload,
      };
    },
  )
  .handleAction(
    actions.loginActions.failure,
    (state: IAuthState, action: any) => {
      // 로그인 실패 액션
      return {
        ...state,
        loading: false,
        errMsgs: action.payload.response.data.errMsgs,
      };
    },
  )
  .handleAction(actions.registerActions.request, (state: IAuthState) => {
    // 회원가입 요청 액션
    return { ...state, loading: true, errMsgs: undefined };
  })
  .handleAction(
    actions.registerActions.success,
    (state: IAuthState, action: any) => {
      // 회원가입 성공 액션
      return {
        ...state,
        loading: false,
        data: action.payload,
      };
    },
  )
  .handleAction(
    actions.registerActions.failure,
    (state: IAuthState, action: any) => {
      // 회원가입 실패 액션
      return {
        ...state,
        loading: false,
        errMsgs: action.payload.response.data.errMsgs,
      };
    },
  )
  .handleAction(
    actions.logoutActions.request,
    (state: IAuthState, action: any) => {
      // 로그아웃 요청 액션
      return {
        ...state,
        logoutLoading: action.payload.loading,
        errMsgs: undefined,
      };
    },
  )
  .handleAction(actions.logoutActions.success, () => {
    // 로그아웃 성공 액션
    return { ...initState, user: null };
  })
  .handleAction(
    actions.logoutActions.failure,
    (state: IAuthState, action: any) => {
      // 로그아웃 실패 액션
      return {
        ...state,
        logoutLoading: false,
        errMsgs: action.payload.response.data.errMsgs,
      };
    },
  )
  .handleAction(actions.meActions.request, (state: IAuthState) => {
    // 내정보 요청 액션
    return { ...state, meLoading: true, user: undefined, errMsgs: undefined };
  })
  .handleAction(actions.meActions.success, (state: IAuthState, action: any) => {
    const {
      payload: { user },
    } = action;

    // 내정보 성공 액션
    return { ...state, meLoading: false, user };
  })
  .handleAction(actions.meActions.failure, (state: IAuthState, action: any) => {
    // 내정보 실패 액션
    return {
      ...state,
      meLoading: false,
      user: null,
      errMsgs: action.payload.response.data.errMsgs,
    };
  });

export default reducer;
