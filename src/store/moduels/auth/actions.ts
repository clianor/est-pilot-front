import { createAction as createSyncAction } from 'typesafe-actions';

import * as loginApi from '../../../api/auth/login';
import * as registerApi from '../../../api/auth/register';
import * as logoutApi from '../../../api/auth/logout';
import * as meApi from '../../../api/auth/me';
import { createAction, createActionEntity } from '../../lib';

/**
 * 초기화 액션
 */
const initAction = createSyncAction('auth/INIT')();

/**
 * 로그인 액션
 */
const loginAction = createAction('auth/LOGIN');
const loginActions = createActionEntity<
  loginApi.IRequest,
  loginApi.IResponse,
  loginApi.IError
>(loginAction);

/**
 * 회원가입 액션
 */
const registerAction = createAction('auth/REGISTER');
const registerActions = createActionEntity<
  registerApi.IRequest,
  registerApi.IResponse,
  registerApi.IError
>(registerAction);

/**
 * 로그아웃 액션
 */
const logoutAction = createAction('auth/LOGOUT');
const logoutActions = createActionEntity<
  logoutApi.IRequest,
  logoutApi.IResponse,
  logoutApi.IError
>(logoutAction);

/**
 * 내정보 액션
 */
const meAction = createAction('auth/ME');
const meActions = createActionEntity<
  meApi.IRequest,
  meApi.IResponse,
  meApi.IError
>(meAction);

const actions = {
  initAction,
  loginActions,
  registerActions,
  logoutActions,
  meActions,
};
export default actions;
