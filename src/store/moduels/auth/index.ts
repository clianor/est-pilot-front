import reducer from './reducer';
import actions from './actions';
import sagas from './sagas';

const auth = {
  reducer,
  actions,
  sagas,
};

export default auth;
