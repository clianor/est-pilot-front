import { combineReducers } from 'redux';

import { combineSagas } from '../lib';

import auth from './auth';
import client from './client';

const moduels = {
  rootReducer: combineReducers({
    auth: auth.reducer,
    client: client.reducer,
  }),
  rootSaga: combineSagas({
    auth: auth.sagas,
  }),
};

export default moduels;
