import { ActionType, createAction } from 'typesafe-actions';

export const CLIENT_LIST = 'client/FETCH_CLIENT_LIST';
export const SET_CLIENT = 'client/SET_CLIENT';
export const SET_PATH = 'client/SET_PATH';
export const SET_DIRECTORY_LIST = 'client/SET_DIRECTORY_LIST';
export const SET_FILE = 'client/SET_FILE';
export const SET_FILE_CHUNK = 'client/SET_FILE_CHUNK';
export const SET_LOADING = 'client/SET_LOADING';
export const CLEAR = 'client/CLEAR';

/**
 * 클라이언트 리스트 셋팅 액션
 */
const client_list = createAction(CLIENT_LIST)<any>();

/**
 * 클라이언트 셋팅 액션
 */
const set_client = createAction(SET_CLIENT)<any>();

/**
 * 경로 셋팅 액션
 */
const set_path = createAction(SET_PATH)<any>();

/**
 * 디렉토리의 리스트 셋팅 액션
 */
const set_directory_list = createAction(SET_DIRECTORY_LIST)<any>();

/**
 * 파일 셋팅 액션
 * - 파일이 설정되는 순간 파일을 클라이언트로 전송 시작.
 */
const set_file = createAction(SET_FILE)<any>();

/**
 * 파일 청크 셋팅 액션
 */
const set_file_chunks = createAction(SET_FILE_CHUNK)<any>();

/**
 * 로딩 셋팅 액션
 */
const set_loading = createAction(SET_LOADING)<boolean>();

/**
 * 클리어 액션
 */
const clear = createAction(CLEAR)();

const actions = {
  client_list,
  set_client,
  set_path,
  set_directory_list,
  set_file,
  set_file_chunks,
  set_loading,
  clear,
};

export type ChatAction = ActionType<typeof actions>;

export default actions;
