import { Reducer } from 'redux';
import { createReducer } from 'typesafe-actions';
import { socket } from '../../../hooks/useSocketIO';

import {
  CLEAR,
  CLIENT_LIST,
  SET_CLIENT,
  SET_DIRECTORY_LIST,
  SET_FILE,
  SET_FILE_CHUNK,
  SET_LOADING,
  SET_PATH,
} from './actions';

interface IClientState {
  file?: File;
  chunk?: number;
  chunks?: number;
  loading: boolean;
  fetching: boolean;
  client?: any;
  path: '/';
  clientList: any[];
  directory: any[];
  tmpDirectory: any[];
}

const initState: IClientState = {
  file: undefined,
  chunk: undefined,
  chunks: undefined,
  loading: false,
  fetching: false,
  client: undefined,
  path: '/',
  clientList: [],
  directory: [],
  tmpDirectory: [],
};

const reducer: Reducer<IClientState> = createReducer<IClientState>(initState)
  .handleAction([CLIENT_LIST], (state: IClientState, action: any) => {
    // 클라이언트 리스트 셋팅 액션
    const clientList = action.payload?.clientList;

    const isExist = clientList.reduce((isExist: boolean, arr: string[]) => {
      return isExist ? true : arr.includes(state.client);
    }, false);

    if (isExist) {
      return { ...state, clientList: clientList };
    }

    return {
      ...initState,
      clientList: clientList,
    };
  })
  .handleAction([SET_CLIENT], (state: IClientState, action: any) => {
    // 클라이언트 셋팅 액션
    socket.emit('getDirectory', action.payload?.client, {
      path: action.payload?.path,
    });
    return {
      ...state,
      client: action.payload?.client,
      path: action.payload?.path,
    };
  })
  .handleAction([SET_PATH], (state: IClientState, action: any) => {
    // 경로 셋팅 액션
    socket.emit('getDirectory', state.client, { path: action.payload?.path });
    return { ...state, path: action.payload?.path };
  })
  .handleAction([SET_DIRECTORY_LIST], (state: IClientState, action: any) => {
    // 디렉토리의 리스트 셋팅 액션
    const { directory, loading, chunk, chunks } = action.payload;

    // 짤려 오는 데이터일 경우
    if (chunk && chunks) {
      if (chunk === chunks) {
        return {
          ...state,
          directory: [...state.tmpDirectory, ...directory],
          tmpDirectory: [],
          loading: loading === undefined ? state.loading : loading,
        };
      }

      return {
        ...state,
        tmpDirectory: [...state.tmpDirectory, ...directory],
        loading: loading === undefined ? state.loading : loading,
      };
    }

    return {
      ...state,
      directory,
      tmpDirectory: [],
      loading: loading === undefined ? state.loading : loading,
    };
  })
  .handleAction([SET_FILE], (state: IClientState, action: any) => {
    // 파일 셋팅 액션
    const { file, chunk, chunks } = action.payload;
    return { ...state, file, chunk, chunks, loading: true };
  })
  .handleAction([SET_FILE_CHUNK], (state: IClientState, action: any) => {
    const { chunk, chunks } = action.payload;
    return { ...state, chunk, chunks };
  })
  .handleAction([SET_LOADING], (state: IClientState, action: any) => {
    // 로딩 셋팅 엑션
    return { ...state, loading: action.payload };
  })
  .handleAction([CLEAR], () => {
    // 클리어 액션
    return { ...initState };
  });

export default reducer;
