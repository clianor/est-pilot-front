import actions from './actions';
import reducer from './reducer';

const chat = { actions, reducer };

export default chat;
