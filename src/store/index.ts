import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import modules from './moduels';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  modules.rootReducer,
  process.env.NODE_ENV === 'production'
    ? applyMiddleware(sagaMiddleware)
    : composeWithDevTools(applyMiddleware(sagaMiddleware)),
);
sagaMiddleware.run(modules.rootSaga);

export default store;

export type RootState = ReturnType<typeof modules.rootReducer>;
