import { call, ForkEffect, put } from 'redux-saga/effects';
import {
  ActionType,
  createReducer,
  createAsyncAction as asyncActionCreator,
  AsyncActionCreatorBuilder,
} from 'typesafe-actions';

export type Action = {
  request: string;
  success: string;
  failure: string;
};

export type PromiseCreatorFunction<P, T> =
  | ((payment: P) => Promise<T>)
  | (() => Promise<T>);

export const createAction = (actionName: string): Action => ({
  request: actionName + '_REQUEST',
  success: actionName + '_SUCCESS',
  failure: actionName + '_FAILURE',
});

export const createActionEntity = <R, S, F>(action: Action) =>
  asyncActionCreator(action.request, action.success, action.failure)<R, S, F>();

export function createCustomReducer<S, A extends { [key: string]: unknown }>(
  state: S,
  action: A,
) {
  type Actions = ActionType<typeof action>;
  type States = typeof state;

  return createReducer<States, Actions>(state);
}

export function createAsyncSaga<
  RequestType,
  RequestPayload,
  SuccessType,
  SuccessPayload,
  FailureType,
  FailurePayload
>(
  asyncAction: AsyncActionCreatorBuilder<
    [RequestType, [RequestPayload, undefined]],
    [SuccessType, [SuccessPayload, undefined]],
    [FailureType, [FailurePayload, undefined]]
  >,
  asyncFunction: PromiseCreatorFunction<RequestPayload, SuccessPayload>,
  successFunc?: any,
  failureFunc?: any,
) {
  return function* saga(action: ReturnType<typeof asyncAction.request>) {
    try {
      const result: SuccessPayload = yield call(
        asyncFunction,
        (action as any).payload,
      );
      yield put(asyncAction.success(result));
      if (successFunc) {
        yield call(successFunc, result);
      }
    } catch (err) {
      yield put(asyncAction.failure(err));
      if (failureFunc) {
        yield call(successFunc, err);
      }
    }
  };
}

export const combineSagas = (sagas: { [key: string]: ForkEffect<never>[] }) =>
  function* (): Generator {
    const targetSagas = Object.values(sagas).flat();
    for (let i = 0; i < targetSagas.length; i++) {
      yield targetSagas[i];
    }
  };
