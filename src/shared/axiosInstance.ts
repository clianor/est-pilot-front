import axios, { AxiosInstance } from 'axios';

const instance: AxiosInstance = axios.create({
  baseURL: process.env.REACT_APP_SERVER_URL,
  timeout: 2000,
  withCredentials: true,
});

// /**
//  * 쿠키 셋팅
//  */
// instance.interceptors.request.use((config: AxiosRequestConfig) => {
//   document.cookie = sessionStorage.getItem("token") || "";
//   return config;
// });

// /**
//  * 쿠키 삭제
//  */
// instance.interceptors.response.use((response: AxiosResponse<any>) => {
//   const cookie = document.cookie;
//   if (cookie) sessionStorage.setItem("token", cookie);
//   clearCookie();
//   return response;
// });

// const clearCookie = () => {
//   document.cookie.split(";").forEach(function (c) {
//     document.cookie = `${
//       c.trim().split("=")[0]
//     }=; expires=Thu, 01 Jan 1999 00:00:10 GMT`;
//   });
// };

export default instance;
