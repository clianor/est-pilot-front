export function pathSplit(path: string): string[] {
  return path.split(/[\\|/]/);
}
